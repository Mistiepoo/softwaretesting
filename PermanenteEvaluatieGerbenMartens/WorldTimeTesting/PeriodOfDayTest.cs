using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PermanenteEvaluatieGerbenMartens;

namespace WorldTimeTesting
{
    [TestClass]
    public class PeriodOfDayTest
    {
        //nacht,voormiddag,middag,namiddag,avond
        [TestMethod]
        public void IP_Returns_Nacht()
        {
            var time = new Mock<IWorldTimeAPI>();
            time.Setup(x => x.GetTimeForLocation()).Returns(3);
            var greeter = new Greeter(time.Object);
            var result = greeter.GetPeriodOfDay();
            Assert.AreEqual("Het is op dit AP adres momenteel nacht", result);
        }

        [TestMethod]
        public void IP_Returns_Voormiddag()
        {
            var time = new Mock<IWorldTimeAPI>();
            time.Setup(x => x.GetTimeForLocation()).Returns(10);
            var greeter = new Greeter(time.Object);
            var result = greeter.GetPeriodOfDay();
            Assert.AreEqual("Het is op dit AP adres momenteel voormiddag", result);
        }

        [TestMethod]
        public void IP_Returns_Middag()
        {
            var time = new Mock<IWorldTimeAPI>();
            time.Setup(x => x.GetTimeForLocation()).Returns(12);
            var greeter = new Greeter(time.Object);
            var result = greeter.GetPeriodOfDay();
            Assert.AreEqual("Het is op dit AP adres momenteel middag", result);
        }

        [TestMethod]
        public void IP_Returns_Namiddag()
        {
            var time = new Mock<IWorldTimeAPI>();
            time.Setup(x => x.GetTimeForLocation()).Returns(15);
            var greeter = new Greeter(time.Object);
            var result = greeter.GetPeriodOfDay();
            Assert.AreEqual("Het is op dit AP adres momenteel namiddag", result);
        }

        [TestMethod]
        public void IP_Returns_Avond()
        {
            var time = new Mock<IWorldTimeAPI>();
            time.Setup(x => x.GetTimeForLocation()).Returns(20);
            var greeter = new Greeter(time.Object);
            var result = greeter.GetPeriodOfDay();
            Assert.AreEqual("Het is op dit AP adres momenteel avond", result);
        }

        //parsen timezone
        [TestMethod]
        public void Timezone_Parse_Returns_Brussels()
        {
            var country = new Mock<IWorldTimeAPI>();
            country.Setup(x => x.GetLocationFromTimezone()).Returns("Europe/Brussels");
            var greeter = new Greeter(country.Object);
            var result = greeter.parseTimezone();
            Assert.AreEqual("Brussels", result);
        }
    }
}
