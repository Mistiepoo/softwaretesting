﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;


namespace PermanenteEvaluatieGerbenMartens
{
    public class WorldTimeAPI : IWorldTimeAPI
    {
        private readonly string ip;
        public WorldTimeAPI(string ip)
        {
            this.ip = ip;
        }

        private string apiUrl = "http://worldtimeapi.org/api/ip/";
        private string weatherApiPrefix = "http://api.openweathermap.org/data/2.5/weather?q=";
        private string weatherApiSuffix = "&appid=b1a90ec4d94d84ecf2a3f2bb634b970d&units=metric";

        public int GetTimeForLocation()
        {
            apiUrl += ip;
            using (var httpClient = new HttpClient())
            {
                var httpResponse = httpClient.GetAsync(apiUrl).GetAwaiter().GetResult();
                var response = httpResponse.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                var fullString = JsonConvert.DeserializeObject<OpenIP>(response).datetime;
                var tLocation = fullString.IndexOf('T');
                var timeString = fullString.Substring(tLocation + 1, 2);
                return Int32.Parse(timeString);
            }
        }

        public string GetLocationFromTimezone()
        {
            using (var httpClient = new HttpClient())
            {
                var httpResponse = httpClient.GetAsync(apiUrl).GetAwaiter().GetResult();
                var response = httpResponse.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                var fullString = JsonConvert.DeserializeObject<OpenIP>(response).timezone;
                return fullString;
            }
        }

        public float GetTempFromCity(string country)
        {
            using (var httpClient = new HttpClient())
            {
                var httpResponse = httpClient.GetAsync(weatherApiPrefix + country + weatherApiSuffix).GetAwaiter().GetResult();
                var response = httpResponse.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                var fullString = JsonConvert.DeserializeObject<OpenWeather>(response).main.temp;

                return fullString;
            }
        }
    }
}
