﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PermanenteEvaluatieGerbenMartens
{
    public class Greeter
    {
        private readonly IWorldTimeAPI worldTimeAPI;
        public Greeter(IWorldTimeAPI worldTimeAPI)
        {
            this.worldTimeAPI = worldTimeAPI;
        }

        public string GetPeriodOfDay()
        {
            int timeOfDay = worldTimeAPI.GetTimeForLocation();
            string returnString = "";

            if (timeOfDay >= 0 && timeOfDay < 7)
            {
                returnString += "Het is op dit AP adres momenteel nacht";
            }

            if (timeOfDay >= 7 && timeOfDay < 12)
            {
                returnString += "Het is op dit AP adres momenteel voormiddag";
            }

            if (timeOfDay >= 12 && timeOfDay < 13)
            {
                returnString += "Het is op dit AP adres momenteel middag";
            }

            if (timeOfDay >= 13 && timeOfDay < 17)
            {
                returnString += "Het is op dit AP adres momenteel namiddag";
            }

            if (timeOfDay >= 17)
            {
                returnString += "Het is op dit AP adres momenteel avond";
            }

            return returnString;
        }

        public string ShowTemp()
        {
            string country = parseTimezone();
            string countryApiString = "";
            float temp = 0;

            switch (country)
            {
                case "Brussels":
                    countryApiString = "Brussels, BE";
                    temp = worldTimeAPI.GetTempFromCity(countryApiString);
                    break;
                case "Chicago":
                    countryApiString = "Chicago, US";
                    temp = worldTimeAPI.GetTempFromCity(countryApiString);
                    break;
                default:
                    countryApiString = "Not Added To Database";
                    temp = 0;
                    break;
            }

            if (temp == 0)
            {
                return "Deze locatie is niet opgeslagen in de database";
            }
            return "Het is op deze locatie " + temp + " graden.";
        }

        public string parseTimezone()
        {
            string fullString = worldTimeAPI.GetLocationFromTimezone();

            int dashLocation = fullString.IndexOf("/");
            string country = fullString.Substring(dashLocation + 1);

            return country;
        }
    }
}
