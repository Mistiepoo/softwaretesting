﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PermanenteEvaluatieGerbenMartens
{
    public interface IWorldTimeAPI
    {
        public int GetTimeForLocation();
        public string GetLocationFromTimezone();
        public float GetTempFromCity(string country);
    }
}
