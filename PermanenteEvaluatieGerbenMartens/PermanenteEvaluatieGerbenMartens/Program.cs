﻿using System;

namespace PermanenteEvaluatieGerbenMartens
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef een IP adres op: ");
            string ip = Console.ReadLine();
            IWorldTimeAPI worldTime = new WorldTimeAPI(ip);
            Greeter worldTimeAPI = new Greeter(worldTime);
            string period = worldTimeAPI.GetPeriodOfDay();
            Console.WriteLine(period);
            Console.WriteLine(worldTimeAPI.ShowTemp());
        }
    }
}
